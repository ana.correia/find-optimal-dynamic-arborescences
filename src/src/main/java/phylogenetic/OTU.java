package phylogenetic;

import exception.PhylogeneticDataException;

import java.util.Arrays;
import java.util.Objects;

public class OTU {

    /**
     * This class represents Operational taxonomic unit
     *
     */

    private int uID; // unique identifier
    private int[] profile; // genetic profile
    private int profileSize; // profile size
    private int currentProfilePosition; // current profile position

    /**
     * Class constructor
     * The profile array is empty
     *
     * @param uID         - unique identifier
     * @param profileSize - size of the profile
     */
    public OTU(int uID, int profileSize) {
        this.uID = uID;
        this.profileSize = profileSize;
        this.profile = new int[this.profileSize];
        this.currentProfilePosition = 0;
    }


    /**
     * Add a profileID to our profile array
     * @param profileID - profile identifier
     * @throws IllegalStateException - if
     */
    public void addLociID(int profileID) {
        if (this.currentProfilePosition == this.profileSize) {
            throw new IllegalStateException("Can not add more data to profile");
        }
        this.setLociIDToPosition(this.currentProfilePosition, profileID);
        this.currentProfilePosition++;
    }

    /**
     * Simple setter for profile id
     * @param position - position
     * @param loci - profile ID
     */
    public void setLociIDToPosition(int position, int loci) {
        this.profile[position] = loci;
    }

    /**
     * Getter for unique identifier parameter
     *
     * @return uID
     */
    public int getuID() {
        return this.uID;
    }

    /**
     * Getter for the profile
     *
     * @return profile
     */
    public int[] getProfile() {
        return this.profile;
    }

    /**
     * Getter for profile size
     *
     * @return profile size
     */
    public int getProfileSize() {
        return this.profileSize;
    }

    public float asymmetricHammingDistance(OTU v) {

        float asymmetricHamming;
        int size = this.getProfileSize();
        int[] profileU = this.getProfile();
        int[] profileV = v.getProfile();
        int Nv = 0;
        int pred = 0;

        for (int i = 0; i < size; i++) {

            int piU = profileU[i];
            int piV = profileV[i];

            if ((piU != piV) && (piV != 0)) {
                pred++;
            }

            if (piV != 0) {
                Nv++;
            }
        }

        if (Nv == 0) {
            throw new PhylogeneticDataException("Nv cannot be zero");
        }

        asymmetricHamming = (float) pred / Nv;
        return asymmetricHamming;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        OTU otu = (OTU) o;
        return uID == otu.uID &&
                profileSize == otu.profileSize &&
                Arrays.equals(profile, otu.profile);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(uID, profileSize);
        result = 31 * result + Arrays.hashCode(profile);
        return result;
    }

    @Override
    public String toString() {
        return "unique id: " + this.uID + " profile: " + Arrays.toString(this.profile);
    }
}
