package cmd;

import algo.directedtree.DirectedTree;
import algo.directedtree.camerini.Camerini;
import comparators.Comparators;
import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import io.IO;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StaticCmd {

    private static final Logger LOGGER = Logger.getLogger(StaticCmd.class.getName());

    public static void main(String[] args) throws IOException {
        run(args);
    }

    /**
     * Run method
     * @param args - cmd arguments
     * @throws IOException - When the input or output file is not found
     */
    private static void run(String[] args) throws IOException {

        Options options = createOptions();
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LOGGER.log(Level.SEVERE, e.toString());
            formatter.printHelp("Available arguments: ", options);
            System.exit(1);
        }

        String inputFilePath = cmd.getOptionValue("input");
        String outputFilePath = cmd.getOptionValue("output");

        boolean phylogenetic = cmd.hasOption("phylogenetic");
        boolean branchOpt = cmd.hasOption("local branch optimization");

        if (!phylogenetic && branchOpt) {
            LOGGER.log(Level.SEVERE,"-l must be used with -p");
            System.exit(-1);
        }

        DirectedGraph directedGraph;
        Comparator<WeightedEdge> edgeComparator;

        if (phylogenetic) {
            directedGraph = IO.phylogeneticDirectedGraph(inputFilePath);
        } else {
            directedGraph = IO.readDirectedGraph(inputFilePath);
        }

        edgeComparator = Comparators.totalOrderCmp();
        DirectedTree directedTree = new Camerini(directedGraph, edgeComparator);
        List<WeightedEdge> weightedEdges = directedTree.getDirectedTree();

        if (branchOpt) {
            // perform branch optimization
            // weightedEdges =
        }

        // finally write solution
        IO.writeDirectedTreeToFile(weightedEdges, outputFilePath);
    }

    /**
     * Create the cmd options
     * @return Options object
     */
    private static Options createOptions() {

        Options options = new Options();
        Option input = new Option("i", "input", true, "Input file path");
        input.setRequired(true);
        options.addOption(input);

        Option output = new Option("o", "output", true, "Output file");
        output.setRequired(true);
        options.addOption(output);

        Option phylogenetic = new Option("p", "phylogenetic", false, "Parse phylogenetic file");
        options.addOption(phylogenetic);

        Option localBranchOptimization = new Option("l", "local branch optimization", false, "Local branch optimization");
        options.addOption(localBranchOptimization);

        return options;
    }
}
