package cmd;

import algo.connectivity.KosarajuSCC;
import algo.directedtree.DynamicDirectedTree;
import algo.directedtree.dynamic.ATree;
import algo.directedtree.dynamic.DynamicCamerini;
import algo.directedtree.dynamic.DynamicTree;
import comparators.Comparators;
import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import exception.DynamicTreeException;
import io.DynamicData;
import io.IO;
import io.Serializer;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DynamicCmd {

    private static final Logger LOGGER = Logger.getLogger(DynamicCmd.class.getName());

    public static void main(String[] args) {
        run(args);
    }

    /**
     * Run method
     * @param args - cmd arguments
     */
    private static void run(String[] args) {

        Options options = createOptions();
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (final ParseException e) {
            LOGGER.log(Level.SEVERE, e.toString());
            formatter.printHelp("Available arguments: ", options);
            System.exit(1);
        }

        boolean dynamicOption = cmd.hasOption("dynamic");
        boolean staticOption = cmd.hasOption("static");

        String inputFilePath = cmd.getOptionValue("input");
        String outputFilePath = cmd.getOptionValue("output");

        boolean addEdges = cmd.hasOption("add edges");
        String addEdgesLst = cmd.getOptionValue("add edges");
        boolean removeEdges = cmd.hasOption("delete edges");
        String removeEdgesLst = cmd.getOptionValue("delete edges");

        try {
            runUtil(staticOption, dynamicOption, inputFilePath, outputFilePath, addEdges, removeEdges, addEdgesLst, removeEdgesLst);
        } catch (final IOException | ClassNotFoundException e ) {
            LOGGER.log(Level.SEVERE, e.toString());
            System.exit(1);
        }
    }

    /**
     * Run the dynamic version of Edmonds algorithm
     * @param staticOption - Boolean value
     * @param dynamicOption - Boolean value
     * @param inputFilePath - Input file
     * @param outputFilePath - Output file
     * @param addEdges - Boolean value
     * @param removeEdges - Boolean value
     * @throws IOException - When reading the graph or saving the dynamic data
     * @throws ClassNotFoundException - Serialization process
     */
    private static void runUtil(boolean staticOption, boolean dynamicOption, String inputFilePath, String outputFilePath, boolean addEdges, boolean removeEdges, String addEdgesLst, String removeEdgesLst) throws IOException, ClassNotFoundException {
        if (staticOption) {
            staticRun(inputFilePath, outputFilePath);
        }
        else if (dynamicOption) {
            dynamicRun(inputFilePath, outputFilePath, addEdges, removeEdges, addEdgesLst, removeEdgesLst);
        }
    }

    /**
     * This function is used the first time we will run the dynamic version of Edmonds algorithm
     * @param inputFilePath - Input file
     * @param outputFilePath - Output file
     * @throws IOException -
     */
    private static void staticRun(String inputFilePath, String outputFilePath) throws IOException {

        DirectedGraph directedGraph = IO.readDirectedGraph(inputFilePath);
        Comparator<WeightedEdge> edgeComparator = Comparators.totalOrderCmp();
        DynamicCamerini dynamicCamerini = new DynamicCamerini(directedGraph, edgeComparator);
        List<WeightedEdge> weightedEdges = dynamicCamerini.getDirectedTree();
        ATree aTree = dynamicCamerini.getATree();
        aTree.resetCameriniForest();

        DynamicData dynamicData = new DynamicData(directedGraph, aTree);
        Serializer<DynamicData> dataSerializer = new Serializer<>();
        dataSerializer.serialize(outputFilePath, dynamicData);

        IO.writeDirectedTreeToFile(weightedEdges, outputFilePath+".txt");

    }

    /**
     * Remove or add the edges in a dynamic way
     * @param inputFilePath - Input file
     * @param outputFilePath - Output file
     * @param addEdges - Boolean value
     * @param removeEdges - Boolean value
     * @throws IOException - File not found
     * @throws ClassNotFoundException - Serialization process
     */
    private static void dynamicRun(String inputFilePath, String outputFilePath, boolean addEdges, boolean removeEdges, String addEdgesLst, String removeEdgesLst) throws IOException, ClassNotFoundException {

        Serializer<DynamicData> serializer = new Serializer<>();
        DynamicData dynamicData = serializer.deserialize(inputFilePath);

        DirectedGraph graph = dynamicData.getDirectedGraph();
        ATree aTree = dynamicData.getATree();
        Comparator<WeightedEdge> edgeComparator = Comparators.totalOrderCmp();

        List<WeightedEdge> directedTree = null;

        // perform addition
        if (addEdges) {
            List<WeightedEdge> edgeList = parseEdges(addEdgesLst);
            System.out.println(edgeList);
            directedTree = dynamicAddition(graph, aTree, edgeList, edgeComparator);
        }
        // perform deletion
        else if (removeEdges){
            List<WeightedEdge> edgeList = parseEdges(removeEdgesLst);
            System.out.println(edgeList);

            directedTree = dynamicRemove(graph, aTree, edgeList, edgeComparator);
        }

        if (directedTree == null) {
            throw new DynamicTreeException("Something went wrong with the dynamic execution");
        }

        // the forest must be
        aTree.resetCameriniForest();
        DynamicData outputDynamicData = new DynamicData(graph, aTree);
        serializer.serialize(outputFilePath, outputDynamicData);
    }

    /**
     * Removes in sequence the edges in the edgeList
     * @param graph - Directed graph
     * @param aTree - Atree data structure
     * @param edgeList - edges to be removed
     * @param edgeComparator - edge comparator object
     * @return Final solution
     */
    private static List<WeightedEdge> dynamicRemove(DirectedGraph graph, ATree aTree, List<WeightedEdge> edgeList, Comparator<WeightedEdge> edgeComparator) {

        List<WeightedEdge> lst = null;
        List<WeightedEdge> removed = new ArrayList<>(edgeList.size());
        for (WeightedEdge edge: edgeList) {
            DynamicDirectedTree dynamicDirectedTree = new DynamicTree(aTree, edgeComparator, graph);
            lst = dynamicDirectedTree.removeEdge(edge);
            removed.add(edge);

            KosarajuSCC kosarajuSCC = new KosarajuSCC(graph, 0);

            if (!kosarajuSCC.isStronglyConnected()) {
                LOGGER.log(Level.WARNING,"After removing: "+ edge + " the graph is no longer strongly connected\nSequence removed:" + removed.toString());
                return lst;
            }

        }

        return lst;
    }

    /**
     * Add in sequence the edges in the edgeList
     * @param graph - Directed graph
     * @param aTree - Atree data structure
     * @param edgeList - edges to be removed
     * @param edgeComparator - edge comparator object
     * @return Final solution
     */
    private static List<WeightedEdge> dynamicAddition(DirectedGraph graph, ATree aTree, List<WeightedEdge> edgeList, Comparator<WeightedEdge> edgeComparator) {

        List<WeightedEdge> lst = null;

        for (WeightedEdge edge: edgeList) {
            DynamicDirectedTree dynamicDirectedTree = new DynamicTree(aTree, edgeComparator, graph);
            lst = dynamicDirectedTree.addNewEdge(edge);
        }

        return lst;
    }

    /**
     * Parses the edges
     * @param data String in the following format "(x,y,w);(a,b,c)"
     * @return A list of WeightedEdge object
     */
    private static List<WeightedEdge> parseEdges(String data) {

        List<WeightedEdge> edgeList = new LinkedList<>();
        String[] edgesStrings = data.split(";");

        for (String edgeStr: edgesStrings) {

            edgeStr = edgeStr.replace("(","");
            edgeStr = edgeStr.replace(")","");

            String[] split = edgeStr.split(",");

            int u = Integer.parseInt(split[0]);
            int v = Integer.parseInt(split[1]);
            Number w = Float.parseFloat(split[2]);

            WeightedEdge edge = new WeightedEdge(u,v,w);
            edgeList.add(edge);
        }
        return edgeList;
    }

    /**
     * Create the cmd options
     * @return Options object
     */
    private static Options createOptions() {

        Options options = new Options();

        OptionGroup optionGroup = new OptionGroup();
        optionGroup.setRequired(true);
        optionGroup.addOption(new Option("d", "dynamic", false, "Run dynamic Edmonds'"));
        optionGroup.addOption(new Option("s", "static", false, "Run the first time Edmonds'"));

        options.addOptionGroup(optionGroup);

        Option input = new Option("i", "input", true, "Input file path");
        optionGroup.setRequired(true);

        options.addOption(input);

        Option output = new Option("o", "output", true, "Output file path");
        output.setRequired(true);

        options.addOption(output);

        OptionGroup operation = new OptionGroup();
        operation.setRequired(false);
        operation.addOption(new Option("add", "add edges", true, "List of edges to be added"));
        operation.addOption(new Option("del", "delete edges", true, "List of edges to deleted"));

        options.addOptionGroup(operation);

        return options;
    }
}
