package datastructures.heaps.binaryheap;


import datastructures.heaps.Heap;

import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

public class BinaryHeap<T> implements Heap<T> {

    private final PriorityQueue<T> priorityQueue;
    private Comparator<T> cmp;

    public BinaryHeap(Comparator<T> cmp) {
        this.cmp = cmp;
        this.priorityQueue = new PriorityQueue<>(this.cmp);
    }

    @Override
    public Comparator<T> comparator() {
        return this.cmp;
    }

    @Override
    public void push(T idx) {
        this.priorityQueue.add(idx);
    }

    @Override
    public void update(T idx, T newIdx) {
        this.priorityQueue.remove(idx);
        this.priorityQueue.add(newIdx);
    }

    @Override
    public void delete(T idx) {
        this.priorityQueue.remove(idx);
    }

    @Override
    public void union(Heap<T> heap) {
        BinaryHeap<T> tGenericBinaryHeap = (BinaryHeap<T>) heap;
        this.priorityQueue.addAll(tGenericBinaryHeap.priorityQueue);
        tGenericBinaryHeap.priorityQueue.clear();
    }

    @Override
    public T top() {
        return this.priorityQueue.peek();
    }

    @Override
    public T pop() {
        return this.priorityQueue.poll();
    }

    @Override
    public int size() {
        return this.priorityQueue.size();
    }

    @Override
    public boolean isEmpty() {
        return this.priorityQueue.isEmpty();
    }

    @Override
    public boolean hasKey(T key) {
        return this.priorityQueue.contains(key);
    }

    @Override
    public void setComparator(Comparator<T> cmp) {
        this.cmp = cmp;
    }

    @Override
    public Set<T> getKeySet() {
        return new HashSet<>(this.priorityQueue);
    }
}
