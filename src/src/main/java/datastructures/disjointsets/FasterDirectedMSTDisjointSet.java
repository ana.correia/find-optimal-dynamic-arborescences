package datastructures.disjointsets;

import java.util.Arrays;

public class FasterDirectedMSTDisjointSet extends DisjointSet implements DirectedMSTDisjointSet {

    private Number[] weight;

    public FasterDirectedMSTDisjointSet(FasterDirectedMSTDisjointSet fasterArboDisjointSet) {
        super(fasterArboDisjointSet);
        this.size = fasterArboDisjointSet.size;
        this.weight = new Number[this.size];
        System.arraycopy(fasterArboDisjointSet.weight, 0, this.weight, 0, this.size);
    }


    public FasterDirectedMSTDisjointSet(int n) {
        super(n);
        this.weight = new Number[this.size];
        for (int i = 0; i < this.size; i++) {
            this.weight[i] = 0.0f;
        }
    }

    @Override
    public int findSet(int i) {

        if (i < 0 || i >= this.size) {
            throw new IllegalArgumentException("i must be positive and less than size");
        }

        for (; this.pi[i] != this.pi[this.pi[i]]; i = this.pi[i]) {
            this.weight[i] = this.weight[i].floatValue() + this.weight[this.pi[i]].floatValue();
            this.pi[i] = this.pi[this.pi[i]];
        }

        return this.pi[i];
    }


    public Number findWeight(int i) {
        Number w = this.weight[i];
        for (; i != this.pi[i]; i = this.pi[i])
            w = w.floatValue() + this.weight[this.pi[i]].floatValue();

        return w;
    }

    @Override
    public void unionSet(int i, int j) {

        if (i < 0 || j < 0 || i >= this.size || j >= this.size)
            throw new IllegalArgumentException();

        int iRoot = this.findSet(i);
        int jRoot = this.findSet(j);

        if (iRoot == jRoot) {
            return;
        }

        if (this.rank[iRoot] > this.rank[jRoot]) {
            this.pi[jRoot] = iRoot;
            this.minusWeight(jRoot, iRoot);
        } else if (this.rank[iRoot] < this.rank[jRoot]) {
            this.pi[iRoot] = jRoot;
            this.minusWeight(iRoot,jRoot);
        } else {
            this.pi[iRoot] = jRoot;
            this.minusWeight(iRoot, jRoot);
            this.rank[jRoot]++;
        }
    }

    private void minusWeight(int i, int j){
        this.weight[i] = this.weight[i].floatValue() - this.weight[j].floatValue();
    }

    public void addWeight(int i, Number w) {
        this.weight[this.findSet(i)] = this.weight[this.findSet(i)].floatValue() +  w.floatValue();
    }

    @Override
    public DirectedMSTDisjointSet clone() {
        return new FasterDirectedMSTDisjointSet(this);
    }

    @Override
    public String toString() {
        return "pi:" + Arrays.toString(pi) + "\nw:" + Arrays.toString(this.weight);
    }
}
