package datastructures.disjointsets;

public interface DirectedMSTDisjointSet {

    int findSet(int i);

    void unionSet(int i, int j);

    Number findWeight(int i);

    void addWeight(int i, Number w);

    boolean sameSet(int i, int j);

    DirectedMSTDisjointSet clone();
}
