package io;

import algo.directedtree.dynamic.ATree;
import datastructures.graph.DirectedGraph;

import java.io.Serializable;

public class DynamicData implements Serializable {

    private static final long serialVersionUID = 452252848494L;

    private DirectedGraph G;
    private ATree aTree;

    public DynamicData(DirectedGraph g, ATree aTree) {
        G = g;
        this.aTree = aTree;
    }

    public DirectedGraph getDirectedGraph() {
        return G;
    }

    public ATree getATree() {
        return aTree;
    }
}
