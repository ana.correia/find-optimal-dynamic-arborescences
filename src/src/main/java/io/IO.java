package io;

import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import exception.PhylogeneticDataException;
import phylogenetic.OTU;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class IO {

    private IO() {}

    /**
     * Reads a file and builds a graph
     *
     * @param fileName - file containing all edges
     * @return graph
     * @throws IOException
     */
    public static DirectedGraph readDirectedGraph(String fileName) throws IOException {

        FileInputStream fStream = new FileInputStream(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(fStream));
        String strLine;

        int i = 0;
        DirectedGraph graph = null;

        while ((strLine = br.readLine()) != null) {
            if (i == 0) {
                String[] strings = strLine.split(" ");
                int size = Integer.parseInt(strings[0]);
                graph = new DirectedGraph(size);
                i++;
            } else {
                WeightedEdge weightedEdge = readGraphUtil(strLine);
                graph.addEdge(weightedEdge);
            }
        }

        fStream.close();
        return graph;
    }

    /**
     * Utility function that parses a line
     * @param strLine - line containing edge information
     * @return WeightedEdge edge object filled with the information in strLine
     */
    private static WeightedEdge readGraphUtil(String strLine) {
        String[] strArr = strLine.split(" ");
        int u = Integer.parseInt(strArr[0]);
        int v = Integer.parseInt(strArr[1]);
        float w = Float.parseFloat(strArr[2]);
        return new WeightedEdge(u, v, w);
    }

    /**
     * Reads a file with an arborescense
     * @param fileName - file name
     * @return List of edges
     * @throws IOException
     */
    public static List<WeightedEdge> readSolution(String fileName) throws IOException {

        FileInputStream fStream = new FileInputStream(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(fStream));
        String strLine;

        List<WeightedEdge> edges = new LinkedList<>();

        while ((strLine = br.readLine()) != null) {
            WeightedEdge edge = readGraphUtil(strLine);
            edges.add(edge);
        }

        fStream.close();
        br.close();
        return edges;
    }

    /**
     * Reads a file containing philogenetic information
     * @param fileName - file name
     * @return List of Otus
     * @throws IOException
     */
    public static List<OTU> parseFile(String fileName) throws IOException {

        List<OTU> otuArrayList = new ArrayList<>();
        FileInputStream fStream = new FileInputStream(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(fStream));
        String strLine;
        int n = 0;
        int len = 0;

        while ((strLine = br.readLine()) != null) {
            String[] strings = strLine.split("\t");

            if (n == 0) {
                len = strings.length;
                n++;
                continue;
            }

            int uID = Integer.parseInt(strings[0]);
            OTU otu = new OTU(uID, len - 1);
            for (int i = 1; i < len; i++) {
                int profileID = Integer.parseInt(strings[i]);
                otu.addLociID(profileID);
            }
            otuArrayList.add(otu);
        }
        fStream.close();
        br.close();
        return otuArrayList;
    }

    public static DirectedGraph phylogeneticDirectedGraph(String fileName) throws IOException {

        List<OTU> otuList = parseFile(fileName);
        int size = otuList.size();

        DirectedGraph directedGraph = new DirectedGraph(otuList.size());

        for (int i = 0; i < size; i++) {
            OTU otuI = otuList.get(i);
            for (int j = 0; j < size; j++) {
                if (i != j) {
                    OTU outJ = otuList.get(j);
                    float h = otuI.asymmetricHammingDistance(outJ);
                    directedGraph.addEdge(new WeightedEdge(i,j,h));
                }
            }
        }

        return directedGraph;
    }

    public static List<Float> harmonicTieBreaking(DirectedGraph graph) {

        int size = graph.size();
        List<Float> htLst = new ArrayList<>(size);

        for (Integer node: graph.getNodeSet()) {

            List<WeightedEdge> neighbors = graph.getNeighbors(node);

            float sum = 0.0f;
            for (WeightedEdge edge: neighbors){
                sum += edge.getWeight().floatValue();
            }

            if (sum == 0.0f) {
                throw new PhylogeneticDataException("Cannot compute ht for node: " + node);
            }

            float inverseSum = 1 / sum;
            float aux = inverseSum / size;
            float ht = 1/aux;
            htLst.add(ht);
        }

        return htLst;
    }

    /**
     * Write a arboresncence to file
     * @param tree - List of edges belonging to the arborescence
     * @param fileName - file name
     * @throws IOException
     */
    public static void writeDirectedTreeToFile(List<WeightedEdge> tree, String fileName) throws IOException {

        File fout = new File(fileName);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        for (WeightedEdge w : tree) {
            String line = w.getSrc() + " " + w.getDest() + " " + w.getWeight();
            bw.write(line);
            bw.newLine();
        }

        bw.close();
    }
}
