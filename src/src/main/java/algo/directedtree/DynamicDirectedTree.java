package algo.directedtree;
import datastructures.graph.edge.WeightedEdge;
import java.util.List;

public interface DynamicDirectedTree {

    List<WeightedEdge> addNewEdge(WeightedEdge newEdge);

    List<WeightedEdge> removeEdge(WeightedEdge removeEdge);

    List<WeightedEdge> updateCost(WeightedEdge edge, Number newCost);
}
