package algo.directedtree;

import datastructures.graph.edge.WeightedEdge;

import java.util.List;

public interface DirectedTree {

    List<WeightedEdge> getDirectedTree();

    Number computeWeight(List<WeightedEdge> edges);
}
