package algo.directedtree.camerini;

import algo.directedtree.dynamic.ATNode;
import datastructures.graph.edge.WeightedEdge;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class CameriniForest implements Serializable {

    private static final long serialVersionUID = 5417492L;

    /**
     * This class represents the camerini proposed in
     * A Note on Finding Optimum Branching, 1979 by: P.M. Camerini, L. Fratta, solutionForest. Maffioli
     * Some changes were made to the contraction phase namely the construction of camerini solutionForest
     */

    private EdgeNode[] pi; //pi array holds the leafs of the tree
    private Set<Integer> rset; // no entering edges
    private int[] maxArr; //hold the destination of the maximum weighted edge in a strongly connected component
    private int nNodes; //number of nodes
    private List<WeightedEdge> solution; //list of weighted edges resultant of processing the forest
    protected Map<WeightedEdge, EdgeNode> edgesInTree;
    public Set<Integer> R; //

    /**
     * Class constructor
     *
     * @param nNodes - number of nodes the size of the pi array
     * @param nEdges - number of edges that its the number of nodes
     */
    public CameriniForest(int nNodes, int nEdges) {
        this.edgesInTree = new HashMap<>(nEdges);
        this.pi = new EdgeNode[nNodes];
        this.rset = new HashSet<>();
        this.maxArr = new int[nNodes];
        this.rset = new HashSet<>();
        this.nNodes = nNodes;
        for (int i = 0; i < nNodes; i++) {
            this.maxArr[i] = i;
        }
    }

    /**
     * Add new node
     * @param edgeNode - edge
     * @return EdgeNode related with edge
     */
    public EdgeNode add(EdgeNode edgeNode) {
        this.edgesInTree.put(edgeNode.getWeightedEdge(), edgeNode);
        return edgeNode;
    }

    /**
     * Add an entry to the pi array
     *
     * @param p    - position
     * @param node - EdgeNode
     */
    public void addPi(int p, EdgeNode node) {
        this.pi[p] = node;
    }

    /**
     * Getter for pi array
     *
     * @return Pi array
     */
    public EdgeNode[] getPi() {
        return this.pi;
    }

    /**
     * Add an element to rset
     *
     * @param root - element
     */
    public void addEntryToRset(int root) {
        this.rset.add(root);
    }

    /**
     * Set an entry to a given position of MaxArr
     *
     * @param position - position
     * @param entry    - entry
     */
    public void setMaxArrPosition(int position, int entry) {
        this.maxArr[position] = entry;
    }

    /**
     * Update Max array
     *
     * @param p1 - position 1
     * @param p2 - position 2
     */
    public void updateMaxArray(int p1, int p2) {
        this.maxArr[p1] = this.maxArr[p2];
    }

    /**
     * Reset the camerini forest
     * Worst-case(O(n))
     */
    public void resetCameriniForest() {
        // reset solution Camerini
        // whenever solution is used to build the MSA the attributes change
        for (EdgeNode node: this.edgesInTree.values()) {
            node.setRemoveF(false);
            node.resetParent();
        }
    }

    /**
     * Clear the nodes with incident edges
     */
    public void clearRset() {
        this.rset.clear();
    }

    /**
     * Reset max array
     */
    public void resetMaxArr(){
        for (int i = 0; i < this.nNodes; i++) {
            this.maxArr[i] = i;
        }
    }

    /**
     * Compute set of Roots
     *
     * @return Set of roots
     */
    private Set<Integer> setR() {

        Set<Integer> R = new HashSet<>();
        for (Integer node: this.rset) {
            R.add(this.maxArr[node]);
        }
        return R;
    }

    /**
     * Find the root of camerini solutionForest
     *
     * @return Set of roots of attribute solutionForest
     */
    protected LinkedList<EdgeNode> rootsF() {
        // store here the roots
        LinkedList<EdgeNode> N = new LinkedList<>();
        for (EdgeNode edgeNode: this.edgesInTree.values()) {
            // edge node is a root if parent == null and isRemoved == false
            if (edgeNode.isRoot()) {
                N.add(edgeNode);
            }
        }

        return N;
    }

    /**
     * Given a edgeNode follow the parent pointer
     * until a root is found
     *
     * @param edgeNode       - edgeNode
     * @param edgeNodesRoots - List of edgeNode
     */
    private void removeFromRoots(EdgeNode edgeNode, List<EdgeNode> edgeNodesRoots) {
        // follow the parent
        for (; edgeNode != null; edgeNode = edgeNode.getParent()) {
            edgeNode.setRemoveF(true);
            for (EdgeNode childEdgeNode : edgeNode.getChildren()) {
                childEdgeNode.setParent(null);
                edgeNodesRoots.add(childEdgeNode);
            }
        }
    }

    /**
     * Implementation  of Algorithm LEAF described in Camerini
     *
     * @return List containing minimum spanning arborescence
     */

    public List<WeightedEdge> expansion() {

        Set<Integer> R = this.setR();
        this.R = R;
        List<WeightedEdge> B = new ArrayList<>(this.nNodes - 1);
        LinkedList<EdgeNode> N = this.rootsF();

        // process R
        for (Integer node : R) {
            if (this.pi[node] != null) {
                this.removeFromRoots(this.pi[node], N);
            }
        }

        // process N
        while (!N.isEmpty()) {
            EdgeNode edgeNode = N.pop();
            if (edgeNode.isRemoveF()) {
                continue;
            }

            WeightedEdge edge = edgeNode.getWeightedEdge();
            B.add(edge);
            int dst = edge.getDest();
            this.removeFromRoots(this.pi[dst], N);
        }

        this.solution = B;
        return B;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (EdgeNode edgeNode: this.edgesInTree.values()) {
            builder.append(edgeNode.toString());
            builder.append("\n");
        }

        return builder.toString();
    }

    /**
     * Getter for solution attribute
     * @return List of weighed edges
     */
    public List<WeightedEdge> getSolution() {
        return this.solution;
    }

    /**
     * Getter for maxArr
     * @return maxArr
     */
    public int[] getMaxArr() {
        return this.maxArr;
    }

    /**
     * Print in a transversal order the tree
     */
    public void printTreeTransversal() {

        for (EdgeNode root: this.rootsF()) {
            List<List<EdgeNode>> transversal = this.printTreeTransversalUtil(root);
            for (List<EdgeNode> edgeNodes: transversal) {

                for (EdgeNode node: edgeNodes) {
                    if (node.getParent() == null) {
                        System.out.print(node.getWeightedEdge() + " (Parent: " + null + ") " + "\t");
                    }
                    else {

                        System.out.print(node.getWeightedEdge() + " (Parent: " + node.getParent().getWeightedEdge() + ") " + "\t");
                    }
                }
                System.out.println();
            }
            System.out.println("---------------------------------------------------");
        }
    }

    /**
     * Util function
     * @param root - root node
     * @return List<List<EdgeNode>>
     */
    private List<List<EdgeNode>> printTreeTransversalUtil(EdgeNode root){

        List<List<EdgeNode>> ans = new ArrayList<>();

        if(root == null)
            return ans;

        Queue<EdgeNode> q = new LinkedList<>();
        q.offer(root);

        while(!q.isEmpty()) {
            int size = q.size();
            List<EdgeNode> list = new ArrayList<>();
            for(int i = 0; i < size; i++) {
                EdgeNode node = q.poll();
                list.add(node);
                for(EdgeNode child: node.getChildren()) {
                    q.offer(child);
                }
            }
            ans.add(list);
        }
        return ans;
    }

    public boolean contains(WeightedEdge removeEdge) {
        return this.getNode(removeEdge) != null;
    }

    public EdgeNode getNode(WeightedEdge weightedEdge) {
       return this.edgesInTree.get(weightedEdge);
    }

    /**
     * Remove edge from the contracted list of an ATNode
     * Worst Case O(n)
     *
     * @param edge - edge to be removed
     */
    public boolean removeEdgeFromContractedEdgeList(ATNode edge) {
        boolean result = false;
        for (EdgeNode edgeNode: this.edgesInTree.values()) {
            ATNode node = (ATNode) edgeNode;
            Set<ATNode> set = node.getContractedEdges();
            if (set.contains(edge)) {
                set.remove(edge);
                result = true;
                break;
            }
        }
        return result;
    }
}
