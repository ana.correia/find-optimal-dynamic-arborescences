package algo.directedtree.camerini;

import datastructures.graph.edge.WeightedEdge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class EdgeNode implements Serializable {

    /**
     * This class represents a node of the camerini
     * A Note on Finding Optimum Branching, 1979 by: P.M. Camerini, L. Fratta, solutionForest. Maffioli
     * Some changes were made to the contraction phase namely the construction of camerini solutionForest
     */

    private static final long serialVersionUID = 4252848494L;

    protected WeightedEdge edge;
    protected EdgeNode parent;
    private List<EdgeNode> children;
    private boolean removeF;
    // reset the tree
    private EdgeNode initialParent;

    /**
     * Class constructor
     *
     * @param edge - WeightedEdge object
     */
    public EdgeNode(WeightedEdge edge) {
        this.edge = edge;
        this.removeF = false;
        this.parent = null;
        this.initialParent = null;
        this.children = new LinkedList<>();
    }

    /**
     * Add a single EdgeNode as a child of the current
     * EdgeNode
     *
     * @param edgeNode - edgeNode to be added
     */
    public void addChild(EdgeNode edgeNode) {
        this.children.add(edgeNode);
    }

    /**
     * Setter for the parent attribute
     *
     * @param parent - parent of current EdgeNode
     */
    public void setParent(EdgeNode parent) {

        if (this.initialParent == null) {
            this.initialParent = parent;
        }

        this.parent = parent;
    }

    public void setInitialParent(EdgeNode initialParent) {
        this.initialParent = initialParent;
    }

    /**
     * Set parent attribute to the initialParent
     */
    public void resetParent() {
        this.parent = this.initialParent;
    }

    /**
     * Check if a given node is a root
     *
     * @return true if EdgeNode does not have a parent, false otherwise
     */
    public boolean isRoot() {
        return this.parent == null;
    }

    /**
     * Return edge of EdgeNode
     *
     * @return Weighted Edge
     */
    public WeightedEdge getWeightedEdge() {
        return this.edge;
    }

    /**
     * WeightedEdge setter
     * @param edge - Weighted Edge object
     */
    public void setWeightedEdge(WeightedEdge edge) {
        this.edge = edge;
    }

    /**
     * Getter for parent attribute
     *
     * @return parent of current node
     */
    public EdgeNode getParent() {
        return this.parent;
    }

    /**
     * Getter for children
     *
     * @return List of child
     */
    public List<EdgeNode> getChildren() {
        return this.children;
    }

    /**
     * Set parameter removeF
     *
     * @param removeF - boolean
     */
    public void setRemoveF(boolean removeF) {
        this.removeF = removeF;
    }

    /**
     * Getter for removedF
     *
     * @return boolean removedF
     */
    public boolean isRemoveF() {
        return this.removeF;
    }

    /**
     * Clear children
     */
    public void clearChildren() {
        this.children.clear();
    }

    /**
     * Override toString method
     *
     * @return String
     */
    @Override
    public String toString() {

        String nodeID = this.getNodeStr(this);
        List<String> childrenID = new ArrayList<>(this.children.size());

        for (EdgeNode edgeNode : this.children) {
            childrenID.add(this.getNodeStr(edgeNode));
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("NodeID: ").append(nodeID);
        if (this.parent != null) {
            stringBuilder.append(" Parent: ").append(this.getNodeStr(this.parent));
        } else {
            stringBuilder.append(" Parent: No parent ");
        }
        stringBuilder.append(" Children: ");

        for (String st : childrenID) {
            stringBuilder.append(" NodeID: ").append(st);
        }

        return stringBuilder.toString();
    }

    /**
     * Simple util method
     *
     * @param edgeNode - edge node
     * @return String
     */
    private String getNodeStr(EdgeNode edgeNode) {
        if (edgeNode.edge != null) {
            return "(u: " + edgeNode.edge.getSrc() + " v: " + edgeNode.edge.getDest() + " w: " + edgeNode.edge.getWeight() + ")";
        } else {
            return "NO EDGE";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EdgeNode edgeNode = (EdgeNode) o;
        return Objects.equals(edge, edgeNode.edge);
    }

    @Override
    public int hashCode() {
        return Objects.hash(edge);
    }
}
