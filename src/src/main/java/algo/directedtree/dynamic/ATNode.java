package algo.directedtree.dynamic;

import algo.directedtree.camerini.EdgeNode;
import datastructures.graph.edge.WeightedEdge;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class ATNode extends EdgeNode implements Serializable {

    /**
     * Node of the Active set Tree structure proposed in
     * Updating Directed Minimum Cost Spanning trees
     * G.Pollatos & Orestis Telelis & Vassilis Zissimopoulos
     */

    private static final long serialVersionUID = 42852848494L;
    public static final Number NO_WEIGHT = Float.MAX_VALUE;
    private int id;

    private boolean superNode;

    private Set<ATNode> contractedEdges;
    private WeightedEdge maxEdgeCycle;

    private Number currentWeight;
    private Number maxCycleW;

    /**
     * Class constructor
     *
     * @param edge - WeightedEdge object
     */
    public ATNode(WeightedEdge edge) {
        super(edge);
        this.contractedEdges = new HashSet<>();
        this.maxCycleW = NO_WEIGHT;
        this.maxEdgeCycle = null;
    }


    /**
     * Class constructor
     *
     * @param edge - WeightedEdge object
     */
    public ATNode(WeightedEdge edge, int id) {
        super(edge);
        this.id = id;
        this.contractedEdges = new HashSet<>();
        this.maxCycleW = NO_WEIGHT;
        this.maxEdgeCycle = null;
    }

    public boolean isSuperNode() {
        return superNode;
    }

    public void setSuperNode(boolean superNode) {
        this.superNode = superNode;
    }

    public Set<ATNode> getContractedEdges() {
        return contractedEdges;
    }

    public WeightedEdge getMaxEdgeCycle() {
        return maxEdgeCycle;
    }

    public Number getMaxCycleW() {
        return maxCycleW;
    }

    public void addToContractedEdges(ATNode edgeNode) {
        this.contractedEdges.add(edgeNode);
    }

    public void addToContractedEdges(Collection<ATNode> edgeNodeCollection) {
        this.contractedEdges.addAll(edgeNodeCollection);
    }

    public void setMaxEdgeCycle(WeightedEdge maxEdgeCycle) {
        this.maxEdgeCycle = maxEdgeCycle;
    }

    public void setMaxCycleW(Number maxCycleW) {
        this.maxCycleW = maxCycleW;
    }

    public Number getCurrentWeight() {
        return currentWeight;
    }

    public void setCurrentWeight(Number currentWeight) {
        this.currentWeight = currentWeight;
    }


    @Override
    public String toString() {

        EdgeNode edgeNode = this.getParent();
        ATNode parent = (ATNode) edgeNode;
        StringBuilder builder = new StringBuilder();

        builder.append("ID: ").append(this.id).append(" Super: ").append(this.superNode).append(" Edge: ").
                append(this.edgeUtil(this.getWeightedEdge())).append(" ").
                append(this.parentUtil(parent)).append("#Contracted edgeList: ").
                append(getContractedEdges()).
                append("CHILDREN: ").
                append(getChildren());

        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
       return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Simple util function to build toString
     *
     * @param edge - edge
     * @return String
     */
    private String edgeUtil(WeightedEdge edge) {
        return edge == null ? "Null" : edge.toString();

    }

    /**
     * Simple util function to build toString
     *
     * @param node - node
     * @return String
     */
    private String parentUtil(ATNode node) {

        StringBuilder res = new StringBuilder();
        res.append("[Parent ");

        if (node == null) {
            res.append("null");
            res.append( "] ");
            return res.toString();
        }

        WeightedEdge edge = node.getWeightedEdge();

        if (edge != null) {
            res.append(" Edge: ").append(edge);
        } else {
            res.append(" Edge: Null");
        }

        res.append( "] ");
        return res.toString();
    }
}
