package algo.directedtree.dynamic;

import algo.directedtree.camerini.CameriniForest;
import algo.directedtree.camerini.EdgeNode;
import datastructures.graph.edge.WeightedEdge;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ATree extends CameriniForest {

    /**
     * Class constructor
     *
     * @param nNodes - number of nodes the size of the pi array
     * @param nEdges - number of edges that its the number of nodes
     */
    public ATree(int nNodes, int nEdges) {
        super(nNodes, nEdges);
    }

    /**
     * Decompose the ATree
     * @param weightedEdge - starting point of tree decomposition
     * @return - List if removed ATNodes
     */
    public List<EdgeNode> decomposeTree(WeightedEdge weightedEdge) {

        List<EdgeNode> removedATNode = new LinkedList<>();

        // The decomposition starts from a Node N where e(N) = weightedEdge
        EdgeNode N = this.getNode(weightedEdge);

        // Find parent of Node N
        // Takes constant time
        EdgeNode parent = N.getParent();

        // Find the affected sets
        while (parent != null) {

            // remove from our list
            this.removeFromATNodeLst(parent);

            // Each of the children of a removed contracted node is made the root of its own subtree
            // I think that this condition always holds
            List<EdgeNode> children = parent.getChildren();
            for (EdgeNode child : children) {
                // child becomes a subtree
                child.setParent(null);
                child.setInitialParent(null);
            }

            // clear children of the parent
            children.clear();
            // store removed c-node
            removedATNode.add(parent);
            // go up in the tree
            parent = parent.getParent();
        }

        return removedATNode;
    }

    /**
     * Perform a reverse order transversal only storing in a stack
     * nodes with children
     * @param root - root node
     * @return Stack of nodes that represent cycle
     */
    public Deque<EdgeNode> reverseOrder(EdgeNode root){

        Deque<EdgeNode> S = new ArrayDeque<>();
        Queue<EdgeNode> Q = new LinkedList<>();
        Q.add(root);

        while (!Q.isEmpty()){
            EdgeNode node = Q.poll();
            if(!node.getChildren().isEmpty()) {
                S.push(node);
            }
            Q.addAll(node.getChildren());
        }

        return S;
    }

    public void removeFromATNodeLst(EdgeNode node) {
        this.edgesInTree.remove(node.getWeightedEdge());
    }

    public void removeFromATNodeLst(WeightedEdge edge) {
      this.edgesInTree.remove(edge);
    }

    /**
     * Override toString method
     *
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (EdgeNode e: this.edgesInTree.values()) {
            ATNode node = (ATNode) e;
            stringBuilder.append(node.toString()).append("\n");
        }
        return stringBuilder.toString();
    }

    /**
     * Add new node
     * @param edgeNode - edge
     * @return EdgeNode related with edge
     */
    @Override
    public EdgeNode add(EdgeNode edgeNode) {
        EdgeNode n = this.getNode(edgeNode.getWeightedEdge());
        if (n != null) {
            n.setInitialParent(null);
            n.setParent(null);
            return n;
        }
        this.edgesInTree.put(edgeNode.getWeightedEdge(), edgeNode);
        return edgeNode;
    }

    /**
     * Find the root of camerini solutionForest
     *
     * @return List of roots of attribute solutionForest
     */
    @Override
    protected LinkedList<EdgeNode> rootsF() {
        // store here the roots
        LinkedList<EdgeNode> N = new LinkedList<>();
        for (EdgeNode edgeNode: this.edgesInTree.values()) {
            ATNode atNode = (ATNode) edgeNode;
            // edge node is a root if parent == null and isRemoved == false
            if (atNode.getParent() == null && atNode.getWeightedEdge() != null) {
                N.add(edgeNode);
            }
        }
        return N;
    }

    /**
     * Find the roots of the atree
     * @return List of Atree roots
     */
    public LinkedList<EdgeNode> roots() {
        // store here the roots
        LinkedList<EdgeNode> N = new LinkedList<>();
        for (EdgeNode edgeNode: this.edgesInTree.values()) {
            ATNode atNode = (ATNode) edgeNode;
            // edge node is a root if parent == null
            if (atNode.getParent() == null) {
                N.add(edgeNode);
            }
        }
        return N;
    }
}
