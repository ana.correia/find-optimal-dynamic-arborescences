package algo.directedtree.dynamic;

import algo.directedtree.DynamicDirectedTree;
import algo.directedtree.camerini.EdgeNode;
import datastructures.disjointsets.DirectedMSTDisjointSet;
import datastructures.disjointsets.FasterDirectedMSTDisjointSet;
import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import exception.DynamicTreeException;

import java.util.ArrayDeque;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DynamicTree implements DynamicDirectedTree {

    private ATree aTree;
    private Comparator<WeightedEdge> cmp;
    private DirectedGraph G;

    /**
     * Class constructor
     * @param aTree - ATree object
     * @param cmp - WeightedEdge comparator
     * @param g - Directed graph
     */
    public DynamicTree(ATree aTree, Comparator<WeightedEdge> cmp, DirectedGraph g) {
        this.aTree = aTree;
        this.aTree.resetCameriniForest();
        this.cmp = cmp;
        this.G = g;
    }

    /**
     * Add a new Edge
     * Worst-case(|V| + ||p|| log |p|)
     *
     * @param newEdge - new edge to bee added
     * @return Minimum Spanning Arborescence
     */
    @Override
    public List<WeightedEdge> addNewEdge(WeightedEdge newEdge) {

        if (newEdge == null) {
            throw new IllegalArgumentException("Argument newEdge is null");
        }

        // hold the final solution
        List<WeightedEdge> solution;

        // Add the new Edge to the graph O(1)
        this.G.addEdge(newEdge);

        int dst = newEdge.getDest();
        int src = newEdge.getSrc();

        EdgeNode[] leafs = this.aTree.getPi();
        EdgeNode nodeDST = leafs[dst];
        EdgeNode nodeSRC = leafs[src];

        // try to find a candidate node worst-case O(n)
        EdgeNode candidate = this.findCandidate(nodeDST, newEdge);

        // no candidate was found, newEdge cannot replace any edge in H
        // so find the LCA and add new edge to the contracted edges
        if (candidate == null) {
            ATNode lca = this.LCA(nodeDST, nodeSRC);
            lca.addToContractedEdges(new ATNode(newEdge));
            solution = this.aTree.getSolution();
            return solution;
        }

        boolean safe = this.isSafeToAdd(candidate, nodeSRC);
        // new Edge is not safe to be added to our tree
        // find the LCA and add edge to the contracted edges list
        if (!safe) {
            ATNode lca = this.LCA(nodeDST, nodeSRC);
            lca.addToContractedEdges(new ATNode(newEdge));
            solution = this.aTree.getSolution();
        } else {
            // perform virtual delete or remove edge
            WeightedEdge candidateEdge = candidate.getWeightedEdge();
            solution = this.removeEdgeUtil(true, candidateEdge, newEdge);
        }

        return solution;
    }

    public ATree getATree() {
        return aTree;
    }

    /**
     * Remove an Edge
     * Worst-case(O(|V| + ||p|| log |p|)
     *
     * @param removeEdge - edge to be removed
     * @return Minimum spanning arborescence
     */
    @Override
    public List<WeightedEdge> removeEdge(WeightedEdge removeEdge) {
        return removeEdgeUtil(false, removeEdge, null);
    }

    /**
     * Dummy update cost function
     *
     * @param edge - original edge
     * @param newCost - new cost regarding the edge parameter
     * @return Minimum spanning arborescence
     */
    @Override
    public List<WeightedEdge> updateCost(WeightedEdge edge, Number newCost) {
        this.removeEdge(edge);
        WeightedEdge newEdge = new WeightedEdge(edge.getSrc(), edge.getDest(), newCost);
        return this.addNewEdge(newEdge);
    }


    /**
     * Remove an edge from the graph and compute new minimum spanning arborescence
     * Utility function
     * Worst-case(O(|V| + ||p|| log |p|)
     *
     * @param virtual - true do not delete edge from graph and contracted, false delete edge from graph
     * @param removeEdge - edge to be removed
     * @param newEdge - new edge to be added
     * @return New Minimum Spanning Arborescence
     */
    private List<WeightedEdge> removeEdgeUtil(boolean virtual, WeightedEdge removeEdge, WeightedEdge newEdge) {

        // check if the edge to be removed belongs to the final solution in average constant time
        boolean inForest = this.aTree.contains(removeEdge);

        // if the edge to be removed is not in our solution implying that the arborescence remains unaffected
        if (!inForest) {
            // remove edge from the contracted-edge list
            this.aTree.removeEdgeFromContractedEdgeList(new ATNode(removeEdge));
            // return MSA
            return this.aTree.getSolution();
        }

        // whenever virtual parameter is true do not perform deletion
        if (!virtual) {
            // remove edge from the digraph O(E)
            this.G.removeWeightedEdge(removeEdge);
            // remove edge from the contracted-edge list
            this.aTree.removeEdgeFromContractedEdgeList(new ATNode(removeEdge));
        }

        // remove the ATNode related with removeEdge
        List<EdgeNode> removedContracted = this.aTree.decomposeTree(removeEdge);

        List<EdgeNode> atNodeRoots = this.aTree.roots();

        // instantiate Edmonds Algorithm
        DynamicCamerini dynamicCamerini = new DynamicCamerini(this.G, this.cmp, this.aTree, atNodeRoots, removedContracted, newEdge, removeEdge);

        // return updated minimum spanning arborescence
        return dynamicCamerini.getDirectedTree();
    }

    /**
     * Find the Lowest Common Ancestor
     * Worst-case O(n)
     *
     * @param nodeSRC - ATNode node
     * @param nodeDST - ATNode node
     * @return LCA of nodeSRC and nodeDST
     */
    private ATNode LCA(EdgeNode nodeSRC, EdgeNode nodeDST) {
        // create an empty HashSet to verify in constant time if node was visited
        Set<EdgeNode> visited = new HashSet<>();
        EdgeNode LCA = null;

        // perform a walk to the root storing the visited nodes in a HashSet
        while (nodeSRC != null) {
            visited.add(nodeSRC);
            nodeSRC = nodeSRC.getParent();
        }

        // when performing the second walk to the root query the HashSet always verifying
        while (nodeDST != null) {
            // if the node is in the visited set then the LCA was found
            if (visited.contains(nodeDST)) {
                LCA = nodeDST;
                break;
            }

            nodeDST = nodeDST.getParent();
        }

        // finally return the LCA
        return (ATNode) LCA;
    }

    /**
     * Find a candidate node
     * Worst-Case O(n)
     *
     * @param parent - first ancestor of the node with dst
     * @param newEdge - new edge
     * @return Candidate Node if it exists, otherwise null
     */
    private EdgeNode findCandidate(EdgeNode parent, WeightedEdge newEdge) {

        WeightedEdge parentEdge = parent.getWeightedEdge();
        ATNode candidate = null;

        if (cmp.compare(parentEdge, newEdge) > 0){
            return parent;
        }

        Set<EdgeNode> ancestors = new HashSet<>();
        EdgeNode cNode = parent;

        while (cNode != null){
            ancestors.add(cNode);
            cNode = cNode.getParent();
        }

        DirectedMSTDisjointSet stronglyConnected = new FasterDirectedMSTDisjointSet(this.G.size());
        List<EdgeNode> roots = this.aTree.roots();

       if (roots.size() > 1) {
           throw new DynamicTreeException("ROOTS > 1 NOT POSSIBLE");
       }

        EdgeNode root = roots.get(0);

        Deque<EdgeNode> nodeStack = this.aTree.reverseOrder(root);

        boolean compare = false;

        while (!nodeStack.isEmpty()) {

            EdgeNode node = nodeStack.pop();

            if (node.getWeightedEdge() == null && node.getParent() == null)
                return null;

            if (ancestors.contains(node)) {
                compare = true;
            }

            ATNode res = this.findCandidateUtil((ATNode)node, stronglyConnected, newEdge, compare);

            compare = false;

            if (res != null) {
                candidate = res;
                break;
            }
        }

        return candidate;
    }

    /**
     * Util find candidate node function
     *
     * @param node - ATNode object
     * @param stronglyConnected - ArboDisjointSet object
     * @param newEdge - WeightedEdge object
     * @param compare - boolean true compare nodes, false does not compare
     * @return Candidate ATNode object
     */
    private ATNode findCandidateUtil(ATNode node, DirectedMSTDisjointSet stronglyConnected , WeightedEdge newEdge, boolean compare){

        WeightedEdge nodeEdge = node.getWeightedEdge();

        List<EdgeNode> children =  node.getChildren();
        Number maxW = node.getMaxCycleW();

        for (EdgeNode child: children) {
            WeightedEdge e = child.getWeightedEdge();
            int dst = stronglyConnected.findSet(e.getDest());
            Number incidentW = e.getOriginalWeight().floatValue() +
                    stronglyConnected.findWeight(e.getDest()).floatValue();
            Number reducedCost = maxW.floatValue() - incidentW.floatValue();
            stronglyConnected.addWeight(dst, reducedCost);
        }

        WeightedEdge newEdgeAdjWeight = this.getWeightedEdgeAdjustedWeight(newEdge, stronglyConnected);
        WeightedEdge nodeEdgeAdjWeight = this.getWeightedEdgeAdjustedWeight(nodeEdge, stronglyConnected);

        if (compare && cmp.compare(nodeEdgeAdjWeight, newEdgeAdjWeight) > 0) {
            return node;
        }

        for (EdgeNode child: children) {
            WeightedEdge e = child.getWeightedEdge();
            int src = e.getSrc();
            int dst = e.getDest();
            stronglyConnected.unionSet(src, dst);
        }

        return null;
    }

    /**
     * Util function that compute
     * @param edge - WeightedEdge object
     * @param directedMSTDisjointSet - DirectedMSTDisjointSet object
     * @return - WeightedEdge with the adjusted weight
     */
    private WeightedEdge getWeightedEdgeAdjustedWeight(WeightedEdge edge, DirectedMSTDisjointSet directedMSTDisjointSet) {
        return new WeightedEdge(edge.getSrc(), edge.getDest(),
                edge.getOriginalWeight().floatValue()
                        + directedMSTDisjointSet.findWeight(edge.getDest()).floatValue());
    }

    /**
     * Verify if the candidate node is safe to be added to our ATree
     * Worst-case O(n)
     *
     * @param candidate - Candidate ATNode
     * @param nodeSRC - Minimum Weight ATNode incident in the source of the edge to be added
     * @return true if nodeSRC is not hanged over candidate, false if it is
     */
    private boolean isSafeToAdd(EdgeNode candidate, EdgeNode nodeSRC) {

        boolean isSafe = true;
        // instantiate queue object
        ArrayDeque<EdgeNode> Q = new ArrayDeque<>();
        // add BFS root element
        Q.add(candidate);
        // perform a BFS checking if nodeSRC is hanged over the candidate node
        while (!Q.isEmpty()) {
            EdgeNode node = Q.poll();
            // if nodeSRC is hanged over the candidate node
            if (nodeSRC.getWeightedEdge().equals(node.getWeightedEdge())) {
                // set isSafe to false
                isSafe = false;
                // break the BFS search
                break;
            }
            List<EdgeNode> children = node.getChildren();
            Q.addAll(children);
        }
        return isSafe;
    }

}
