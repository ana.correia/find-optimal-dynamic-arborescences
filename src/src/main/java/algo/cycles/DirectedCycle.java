package algo.cycles;

import java.util.Stack;

public interface DirectedCycle {

    Stack<Integer> cycle();
    boolean hasCycle();
}
