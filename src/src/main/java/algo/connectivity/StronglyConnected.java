package algo.connectivity;

public interface StronglyConnected {

    public boolean isStronglyConnected();
}
