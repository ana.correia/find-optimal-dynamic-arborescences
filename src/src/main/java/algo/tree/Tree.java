package algo.tree;


import datastructures.graph.edge.WeightedEdge;

import java.util.List;

public interface Tree {

    List<WeightedEdge> getTree();

    Number computeWeight(List<WeightedEdge> lst);
}
