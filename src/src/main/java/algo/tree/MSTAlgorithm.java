package algo.tree;


import datastructures.graph.UndirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import datastructures.heaps.Heap;
import datastructures.heaps.binomial.BinomialHeap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class MSTAlgorithm implements Tree {

    private Heap<Integer> q;
    private int[] s;
    private int[] pi;
    private WeightedEdge[] d;
    private UndirectedGraph g;
    private Comparator<WeightedEdge> cmp;
    private Comparator<Integer> icmp;

    /**
     * Class constructor
     * Worst case is O(n)
     * This constructor uses by default Fast Rank Relaxed Heap
     *
     * @param g - input graph
     * @param cmp - edge comparator
     */
    public MSTAlgorithm(UndirectedGraph g, Comparator<WeightedEdge> cmp) {

        this.g = g;
        this.cmp = cmp;

        this.s = new int[this.g.size()];
        this.pi = new int[this.g.size()];
        this.d = new WeightedEdge[this.g.size()];

        for (int i = 0; i < this.g.size(); i++) {
            this.s[i] = 0;
            this.pi[i] = -1;
            this.d[i] = null;
        }

        this.icmp = (u, v) -> cmp.compare(d[u], d[v]);
        this.q = new BinomialHeap<>(this.icmp);
    }


    /**
     * Computes the minimum spanning tree of a given graph
     *
     * @return Array of edges containing the minimum spanning tree
     */
    @Override
    public List<WeightedEdge> getTree() {

        List<WeightedEdge> weightedEdges = new ArrayList<>(this.g.size() - 1);

        // We take node 0 as root or src.
        this.d[0] = null;
        this.q.push(0);

        while (!this.q.isEmpty()) {

            int u = this.q.pop();
            this.s[u] = 1;

            if (this.pi[u] >= 0)
                weightedEdges.add(this.d[u]);

            List<WeightedEdge> neighbors = this.g.getNeighbors(u);

            for (WeightedEdge e : neighbors) {

                int v = e.getDest();

                if (this.s[v] == 1)
                    continue;

                // Relax
                if (this.d[v] == null || (this.cmp.compare(this.d[v], e) > 0)) {
                    this.d[v] = e;
                    this.pi[v] = u;
                    this.q.push(v);
                }
            }
        }

        return weightedEdges;
    }

    /**
     * Getter for queue comparator
     *
     * @return q
     */
    public Comparator<Integer> getIcmp() {
        return icmp;
    }

    @Override
    public Number computeWeight(List<WeightedEdge> tree) {
        float res = 0;
        for (WeightedEdge weightedEdge : tree) {
            res += weightedEdge.getWeight().floatValue();
        }
        return res;
    }
}
