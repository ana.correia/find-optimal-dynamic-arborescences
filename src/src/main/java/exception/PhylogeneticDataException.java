package exception;

public class PhylogeneticDataException extends RuntimeException {

    public PhylogeneticDataException(String message) {
        super(message);
    }
}
