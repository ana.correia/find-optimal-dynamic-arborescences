package datastructures.graph;

import datastructures.graph.edge.WeightedEdge;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class DirectedGraphTest {

    private DirectedGraph directedGraph;

    @BeforeEach
    public void setup() {
        this.directedGraph = new DirectedGraph();
    }

    @Test
    public void testEdgeInsertion() {

        WeightedEdge edge1 = new WeightedEdge(0,1,20);
        WeightedEdge edge2 = new WeightedEdge(1,2,4);
        WeightedEdge edge3 = new WeightedEdge(0,2,-3);
        WeightedEdge edge4 = new WeightedEdge(0,3,44);

        this.directedGraph.addEdge(edge1);
        this.directedGraph.addEdge(edge2);
        this.directedGraph.addEdge(edge3);
        this.directedGraph.addEdge(edge4);

        assertEquals(4, directedGraph.size());
        assertEquals(4, directedGraph.edges());

        List<WeightedEdge> n = directedGraph.getNeighbors(0);
        assertEquals(3, n.size());
        assertTrue(n.contains(edge1));
        assertTrue(n.contains(edge4));
        assertTrue(n.contains(edge3));

        n = directedGraph.getNeighbors(1);
        assertEquals(1, n.size());
        assertTrue(n.contains(edge2));

    }

    @Test
    public void testReverseGraph() {

        WeightedEdge edge1 = new WeightedEdge(0,1,20);
        WeightedEdge edge2 = new WeightedEdge(1,2,4);
        WeightedEdge edge3 = new WeightedEdge(0,2,-3);
        WeightedEdge edge4 = new WeightedEdge(0,3,44);

        this.directedGraph.addEdge(edge1);
        this.directedGraph.addEdge(edge2);
        this.directedGraph.addEdge(edge3);
        this.directedGraph.addEdge(edge4);

        this.directedGraph.reverseGraph();

        assertEquals(4, directedGraph.size());
        assertEquals(4, directedGraph.edges());

        List<WeightedEdge> n = directedGraph.getNeighbors(0);
        assertEquals(0, n.size());

        n = directedGraph.getNeighbors(2);
        assertTrue(n.contains(edge3));
        assertTrue(n.contains(edge2));
    }

    @Test
    public void testGraphGetNodeSet() {

        WeightedEdge edge1 = new WeightedEdge(0,1,20);
        WeightedEdge edge2 = new WeightedEdge(1,2,4);
        WeightedEdge edge3 = new WeightedEdge(0,2,-3);
        WeightedEdge edge4 = new WeightedEdge(0,3,44);

        this.directedGraph.addEdge(edge1);
        this.directedGraph.addEdge(edge2);
        this.directedGraph.addEdge(edge3);
        this.directedGraph.addEdge(edge4);

        Set<Integer> nodes = new HashSet<>();
        nodes.add(0); nodes.add(1); nodes.add(2); nodes.add(3);

        assertEquals(nodes, this.directedGraph.getNodeSet());
    }

    @Test
    public void testGraphGetAllEdges() {

        WeightedEdge edge1 = new WeightedEdge(0,1,20);
        WeightedEdge edge2 = new WeightedEdge(1,2,4);
        WeightedEdge edge3 = new WeightedEdge(0,2,-3);
        WeightedEdge edge4 = new WeightedEdge(0,3,44);

        this.directedGraph.addEdge(edge1);
        this.directedGraph.addEdge(edge2);
        this.directedGraph.addEdge(edge3);
        this.directedGraph.addEdge(edge4);

        List<WeightedEdge> weightedEdges = new LinkedList<>();
        weightedEdges.add(edge1);
        weightedEdges.add(edge2);
        weightedEdges.add(edge3);
        weightedEdges.add(edge4);

        List<WeightedEdge> allEdges = this.directedGraph.getAllEdges();
        assertEquals(new HashSet<>(weightedEdges), new HashSet<>(allEdges));
    }

    @Test
    public void testGraphRemoveEdge() {

        WeightedEdge edge1 = new WeightedEdge(0,1,20);
        WeightedEdge edge2 = new WeightedEdge(1,2,4);
        WeightedEdge edge3 = new WeightedEdge(0,2,-3);
        WeightedEdge edge4 = new WeightedEdge(0,3,44);

        this.directedGraph.addEdge(edge1);
        this.directedGraph.addEdge(edge2);
        this.directedGraph.addEdge(edge3);
        this.directedGraph.addEdge(edge4);

        this.directedGraph.removeEdge(edge1);
        assertFalse(this.directedGraph.getAllEdges().contains(edge1));
        assertEquals(3, this.directedGraph.edges());

        this.directedGraph.removeEdge(edge2);
        assertFalse(this.directedGraph.getAllEdges().contains(edge2));
        assertEquals(2, this.directedGraph.edges());

        this.directedGraph.removeEdge(edge3);
        assertFalse(this.directedGraph.getAllEdges().contains(edge3));
        assertEquals(1, this.directedGraph.edges());

        this.directedGraph.removeEdge(edge4);
        assertFalse(this.directedGraph.getAllEdges().contains(edge4));
        assertEquals(0, this.directedGraph.edges());
    }
}
