package datastructures.graph;

import datastructures.graph.edge.WeightedEdge;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class WeightedEdgeTest {

  @Test
  public void testCreateEdge() {
      WeightedEdge edge = new WeightedEdge(1, 2, 10);
      assertEquals(1, edge.getSrc());
      assertEquals(2, edge.getDest());
      assertEquals(10, edge.getWeight());
  }

  @Test
  public void testCompareEdge() {
      WeightedEdge edge1 = new WeightedEdge(1, 2, 10);
      WeightedEdge edge2 = new WeightedEdge(1, 2, 10);
      assertEquals(edge1, edge2);
    }
}
