package datastructures.heaps;

import datastructures.heaps.binaryheap.BinaryHeap;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BinaryHeapTest extends AbstractHeapTest {

    @BeforeEach
    public void setup() {
        setupUtil();
        this.heap = new BinaryHeap<>(Integer::compareTo);
    }

    @Override
    public void testHeapUnion() {
        this.populateHeap();
        assertEquals(this.heap.size(), this.data.size());

        BinaryHeap<Integer> other = new BinaryHeap<>(Integer::compareTo);

        for (int i = 0; i < 20; i++) {
            int t = rand.nextInt();
            other.push(t);
            this.sortedData.add(t);
        }

        this.sortedData.sort(Integer::compareTo);
        this.heap.union(other);

        assertEquals(this.sortedData.size(), this.heap.size());

        for (Integer v: this.sortedData) {
            assertEquals(v, this.heap.pop());
        }
    }
}
