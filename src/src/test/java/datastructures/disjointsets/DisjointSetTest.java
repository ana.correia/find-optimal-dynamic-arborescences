package datastructures.disjointsets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DisjointSetTest {

    private DisjointSet disjointSet;
    private int n;

    @BeforeEach
    public void setup() {

        this.n = 10;
        this.disjointSet = new DisjointSet(this.n);
    }

    @Test
    public void testFindWithoutAnyUnion() {

        for (int i = 0; i < this.n; i++) {
            assertEquals(i, this.disjointSet.findSet(i));
        }
    }

    @Test
    public void testFindSetNegativeValue() {

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
           this.disjointSet.findSet(-1);
        });

    }

    @Test
    public void testFindSetLargerThanSize() {

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            this.disjointSet.findSet(this.n + 1);
        });
    }

    @Test
    public void testUnionSetSimpleUnion() {
        this.disjointSet.unionSet(0,1);
        assertEquals(1, this.disjointSet.findSet(0));
    }

    @Test
    public void testUnion() {

        this.disjointSet.unionSet(0,1);
        assertEquals(1, this.disjointSet.findSet(0));
        assertEquals(1, this.disjointSet.findSet(1));

        this.disjointSet.unionSet(0,2);
        assertEquals(1, this.disjointSet.findSet(2));
        assertEquals(1, this.disjointSet.findSet(0));
        assertEquals(1, this.disjointSet.findSet(1));

        this.disjointSet.unionSet(8,9);
        assertEquals(9, this.disjointSet.findSet(9));

        this.disjointSet.unionSet(9,7);
        assertEquals(9, this.disjointSet.findSet(7));

        this.disjointSet.unionSet(8,5);
        assertEquals(9, this.disjointSet.findSet(5));

        assertEquals(4, this.disjointSet.findSet(4));
    }
}
