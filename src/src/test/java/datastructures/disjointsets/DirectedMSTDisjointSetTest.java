package datastructures.disjointsets;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DirectedMSTDisjointSetTest {

    private DirectedMSTDisjointSet disjointSet;
    private int n;

    @BeforeEach
    public void setup() {

        this.n = 10;
        this.disjointSet = new FasterDirectedMSTDisjointSet(this.n);
    }

    @Test
    public void testFindWeightNotInitialized() {

        for (int i = 0; i < this.n; i++) {
            assertEquals(0.0f, this.disjointSet.findWeight(i));
        }
    }

    @Test
    public void testFindWeightSimple() {
       this.disjointSet.addWeight(0, 10.0f);
       assertEquals(10.0f, this.disjointSet.findWeight(0));
    }

    @Test
    public void testUnion() {
        this.disjointSet.addWeight(0, 10.0f);
        this.disjointSet.addWeight(1, 15.0f);
        this.disjointSet.unionSet(0,1);
        assertEquals(1, this.disjointSet.findSet(1));
        assertEquals(1, this.disjointSet.findSet(0));
    }
}
