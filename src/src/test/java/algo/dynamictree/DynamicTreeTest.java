package algo.dynamictree;

import algo.connectivity.KosarajuSCC;
import algo.directedtree.DynamicDirectedTree;
import algo.directedtree.camerini.Camerini;
import algo.directedtree.dynamic.ATree;
import algo.directedtree.dynamic.DynamicCamerini;
import algo.directedtree.dynamic.DynamicTree;
import comparators.Comparators;
import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import io.Directory;
import io.IO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DynamicTreeTest {

    private Map<String, DirectedGraph> graphs;
    private Map<String, List<WeightedEdge>> solutions;
    private Comparator<WeightedEdge> cmp;
    private Random rand;

    @BeforeAll
    public void setup() throws IOException {

        this.rand = new Random();
        this.graphs = new HashMap<>();
        this.solutions = new HashMap<>();
        this.cmp = Comparators.totalOrderCmp();

        String[] files = new String[]{"10_complete_graph_K5.txt",
                                      "12_asymmetric.txt", "05_large_cycle.txt",
                                      "09_nested_cycles.txt", "11_worst_case.txt"};

        for (String file: files) {
            DirectedGraph graph = IO.readDirectedGraph(Directory.TEST_GRAPH + file);
            List<WeightedEdge> solution = IO.readSolution(Directory.TEST_GRAPH_SOLUTION + file);
            this.graphs.put(file, graph);
            this.solutions.put(file, solution);
        }
    }

    @Test
    public void testDeleteCompleteK5() {
        DirectedGraph graph = this.graphs.get("10_complete_graph_K5.txt");
        List<WeightedEdge> solution = this.solutions.get("10_complete_graph_K5.txt");
        this.performDeletion(new DirectedGraph(graph), solution);
    }

    @Test
    public void testDeleteAsymmetric() {
        DirectedGraph graph = this.graphs.get("12_asymmetric.txt");
        List<WeightedEdge> solution = this.solutions.get("12_asymmetric.txt");
        this.performDeletion(new DirectedGraph(graph), solution);
    }

    @Test
    public void testDeleteLargeCycle() {
        DirectedGraph graph = this.graphs.get("05_large_cycle.txt");
        List<WeightedEdge> solution = this.solutions.get("05_large_cycle.txt");
        this.performDeletion(new DirectedGraph(graph), solution);
    }

    @Test
    public void testDeleteNestedCycle() {
        DirectedGraph graph = this.graphs.get("09_nested_cycles.txt");
        List<WeightedEdge> solution = this.solutions.get("09_nested_cycles.txt");
        this.performDeletion(new DirectedGraph(graph), solution);
    }

    @Test
    public void testDeleteWorstCase() {
        DirectedGraph graph = this.graphs.get("11_worst_case.txt");
        List<WeightedEdge> solution = this.solutions.get("11_worst_case.txt");
        this.performDeletion(new DirectedGraph(graph), solution);
    }

    @Test
    public void testAddCompleteK5() {
        DirectedGraph graph = this.graphs.get("10_complete_graph_K5.txt");
        List<WeightedEdge> solution = this.solutions.get("10_complete_graph_K5.txt");

        List<WeightedEdge> weightedEdgeList = Arrays.asList(new WeightedEdge(4,2,2.0f),
                new WeightedEdge(0,4,1.0f), new WeightedEdge(0,3,2.0f), new WeightedEdge(1,2,1.0f));
        this.performAddition(new DirectedGraph(graph), solution, weightedEdgeList);
    }

    @Test
    public void testAddLargeCycle() {
        DirectedGraph graph = this.graphs.get("05_large_cycle.txt");
        List<WeightedEdge> solution = this.solutions.get("05_large_cycle.txt");
        List<WeightedEdge> weightedEdgeList = Arrays.asList(new WeightedEdge(2,0,1.0f),
                new WeightedEdge(3,2,1.0f), new WeightedEdge(4,1,2.0f));
        this.performAddition(new DirectedGraph(graph), solution, weightedEdgeList);
    }

    @Test
    public void testAddNestedCycle() {
        DirectedGraph graph = this.graphs.get("09_nested_cycles.txt");
        List<WeightedEdge> solution = this.solutions.get("09_nested_cycles.txt");
        List<WeightedEdge> weightedEdgeList = Arrays.asList(new WeightedEdge(3,2,2.0f),
                new WeightedEdge(1,3,1.0f), new WeightedEdge(1,4,2.0f));
        this.performAddition(new DirectedGraph(graph), solution, weightedEdgeList);
    }

    @Test
    public void testAddWorstCase() {
        DirectedGraph graph = this.graphs.get("11_worst_case.txt");
        List<WeightedEdge> solution = this.solutions.get("11_worst_case.txt");
        List<WeightedEdge> weightedEdgeList = Arrays.asList(new WeightedEdge(3,2,2.0f),
                new WeightedEdge(1,4,2.0f));
        this.performAddition(new DirectedGraph(graph), solution, weightedEdgeList);
    }

    @Test
    public void testAddAsymmetric() {
        DirectedGraph graph = this.graphs.get("12_asymmetric.txt");
        List<WeightedEdge> solution = this.solutions.get("12_asymmetric.txt");
        List<WeightedEdge> weightedEdgeList = Arrays.asList(new WeightedEdge(1,2,1.0f),
                new WeightedEdge(2,4,2.0f), new WeightedEdge(1,4,2.0f));
        this.performAddition(new DirectedGraph(graph), solution, weightedEdgeList);
    }

    public void performDeletion(DirectedGraph graph, List<WeightedEdge> solution) {

        // first we assert that our dynamic version is computing the correct arborescense
        DirectedGraph copy = new DirectedGraph(graph);
        DynamicCamerini dynamicCamerini = new DynamicCamerini(graph, this.cmp);
        List<WeightedEdge> dynamicSolution = dynamicCamerini.getDirectedTree();
        ATree aTree = dynamicCamerini.getATree();
        aTree.resetCameriniForest();

        Assertions.assertEquals(new HashSet<>(solution), new HashSet<>(dynamicSolution));
        Assertions.assertEquals(solution.size(), dynamicSolution.size());

        do {
            // choose randomly an edge that will have impact if removed
            WeightedEdge weightedEdge = dynamicSolution.get(rand.nextInt(dynamicSolution.size()));
            System.out.println("Remove: " + weightedEdge);
            DynamicTree dynamicDirectedTree = new DynamicTree(aTree, this.cmp, graph);
            dynamicSolution = dynamicDirectedTree.removeEdge(weightedEdge);

            copy.removeEdge(weightedEdge);
            Camerini camerini = new Camerini(copy, this.cmp);
            List<WeightedEdge> cameriniSolution = camerini.getDirectedTree();

            Assertions.assertEquals(camerini.computeWeight(cameriniSolution), camerini.computeWeight(dynamicSolution));
            Assertions.assertEquals(cameriniSolution.size(), dynamicSolution.size());

        // It only works when the graph is strongly connected
        } while (new KosarajuSCC(copy,0).isStronglyConnected());

    }

    public void performAddition(DirectedGraph graph, List<WeightedEdge> solution, List<WeightedEdge> weightedEdgeList) {
        // first we assert that our dynamic version is computing the correct arborescense
        DirectedGraph copy = new DirectedGraph(graph);
        System.out.println(graph);
        DynamicCamerini dynamicCamerini = new DynamicCamerini(graph, this.cmp);
        List<WeightedEdge> dynamicSolution = dynamicCamerini.getDirectedTree();
        ATree aTree = dynamicCamerini.getATree();

        Assertions.assertEquals(new HashSet<>(solution), new HashSet<>(dynamicSolution));
        Assertions.assertEquals(solution.size(), dynamicSolution.size());

        // let us add always an edge that must substitute another edge in the atree
        for (WeightedEdge newEdge: weightedEdgeList) {

            System.out.println("ADDING: " + newEdge);

            DynamicDirectedTree dynamicDirectedTree = new DynamicTree(aTree, this.cmp, graph);
            dynamicSolution = dynamicDirectedTree.addNewEdge(newEdge);

            copy.addEdge(newEdge);
            Camerini camerini = new Camerini(copy, this.cmp);
            List<WeightedEdge> cameriniSolution = camerini.getDirectedTree();

            Assertions.assertEquals(camerini.computeWeight(cameriniSolution), camerini.computeWeight(dynamicSolution));
            Assertions.assertEquals(cameriniSolution.size(), dynamicSolution.size());
        }
    }

}
