package algo.directedtree;

import algo.directedtree.dynamic.DynamicCamerini;
import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import org.junit.jupiter.api.TestInstance;

import java.util.Comparator;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DynamicCameriniTest extends AbstractDirectedTreeTest {

    @Override
    public DirectedTree createDirectedTree(DirectedGraph graph, Comparator<WeightedEdge> comparator) {
        return new DynamicCamerini(graph, comparator);
    }
}
