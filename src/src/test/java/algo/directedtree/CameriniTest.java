package algo.directedtree;

import algo.directedtree.camerini.Camerini;
import datastructures.graph.DirectedGraph;
import datastructures.graph.edge.WeightedEdge;
import org.junit.jupiter.api.TestInstance;

import java.util.Comparator;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CameriniTest extends AbstractDirectedTreeTest {

    @Override
    public DirectedTree createDirectedTree(DirectedGraph graph, Comparator<WeightedEdge> comparator) {
        return new Camerini(graph, comparator);
    }
}
