# On finding optimal (dynamic) arborescences

Given a directed weighted and connected graph G, we address the problem of finding an optimal arborescence on G. Although there are known algorithms for this problem, with the one proposed by Edmonds being the most well known, there are several proposed algorithmic ideas around this problem that have not been used in practical implementations. In this paper we gather and explore those ideas, providing practical implementations and experimental results. We address also the problem of maintaining an optimal arborescence on a dynamic graph G.
